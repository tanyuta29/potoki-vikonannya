package exchange;

public class Share {
    private String name;
    private double amount;
    private double price;

    public Share(String name, double amount, double price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public Share() {
    }

    public double getPrice() {
        return price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}