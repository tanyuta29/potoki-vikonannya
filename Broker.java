package exchange;

import java.time.LocalTime;
import java.util.Random;

public class Broker implements Runnable {
    private String name;
    private double budget;
    private double buyThreshold;
    private double sellThreshold;

    public Broker() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSellThreshold(double sellThreshold) {
        this.sellThreshold = sellThreshold;
    }

    public void setBuyThreshold(double buyThreshold) {
        this.buyThreshold = buyThreshold;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getSellThreshold() {
        return sellThreshold;
    }

    public double getBuyThreshold() {
        return buyThreshold;
    }

    public double getBudget() {
        return budget;
    }

    public Broker(String name, double budget, double buyThreshold, double sellThreshold) {
        this.name = name;
        this.budget = budget;
        this.buyThreshold = buyThreshold;
        this.sellThreshold = sellThreshold;
    }


    public synchronized void buyShare(Share share) {
        Broker broker = new Broker();
        if (share.getPrice() <= buyThreshold && budget >= share.getPrice()) {
            double amount = (int) (budget / share.getPrice());
            if (amount > share.getAmount()) {
                amount = share.getAmount();
            }
            double cost = amount * share.getPrice();
            budget -= cost;
            broker.sellStock(share);
            System.out.println(LocalTime.now() + "Спроба купівлі для акції " + name + " успішна." + "Куплено " + amount + " акцій ");
        }
    }

    public synchronized void sellStock(Share share) {
        Broker broker = new Broker();
        if (share.getPrice() >= sellThreshold && share.getAmount() > 0) {
            double amount = (int) (budget / share.getPrice());
            if (amount > share.getAmount()) {
                amount = share.getAmount();
            }
            double income = amount * share.getPrice();
            budget += income;
            broker.buyShare(share);
            System.out.println(LocalTime.now() + name + " продано " + amount + " акцій для " + share.getName());

            if (amount < share.getAmount()) {
                System.out.println(LocalTime.now() + "Спроба купівлі акції для " + share + " для " + name + " не успішна.");
            }
        }

    }

    @Override
    public void run() {
        Share share1 = new Share();
        while (true) {
            try {
                Thread.sleep(3000);
                Random rand = new Random();
                int stockIndex = rand.nextInt(Exchange.getShares().size());
                Share share = Exchange.getShares().get(stockIndex);
                if (rand.nextDouble() < 0.5) {
                    buyShare(share);
                } else {
                    sellStock(share);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(LocalTime.now() + "Ціна акції змінилася. Поточна вартість: " + share1.getPrice());
        }
    }
}
