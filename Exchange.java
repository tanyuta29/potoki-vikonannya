package exchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Exchange {

    private static List<Share> shares = new ArrayList<>();
    private static List<Broker> brokers = new ArrayList<>();

    public static void addStock(Share share) {
        shares.add(share);
    }

    public static List<Share> getShares() {
        return shares;
    }

    public static void addBroker(Broker broker) {
        brokers.add(broker);
    }

    public static List<Broker> getBrokers() {
        return brokers;
    }

}
