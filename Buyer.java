package exchange;

import java.util.Arrays;
import java.util.List;

public class Buyer {
    private String name;
    private List<Target> targets;

    public Buyer(String name, Target... targets) {
        this.name = name;
        this.targets = Arrays.asList(targets);
    }

    public String getName() {
        return name;
    }

    public List<Target> getTargets() {
        return targets;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTargets(List<Target> targets) {
        this.targets = targets;
    }
}
