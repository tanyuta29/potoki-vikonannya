package exchange;

public class Main {
    public static void main(String[] args) {

        Share appleShare = new Share("AAPL", 141, 100);
        Share cokeShare = new Share("COKE", 387, 1000);
        Share ibmShare = new Share("IBM", 137, 200);

        Buyer Alice = new Buyer("Alice", new TargetBuy("AAPL", 10, 100), new TargetBuy("COKE", 20, 390));
        Buyer Bob = new Buyer("Bob", new TargetBuy("AAPL", 10, 140), new TargetBuy("IBM", 20, 135));
        Buyer Charlie = new Buyer("Charlie", new TargetBuy("COKE", 300, 370));

    }

}
